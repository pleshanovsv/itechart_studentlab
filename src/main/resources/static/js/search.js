/* */
var SearchController = {
    phoneCounter: 0,
    gender: [],
    personPhoto: "media/person.jpg",
    contactPhoto: {},

    execute: function() {
        var _this = this;
        this.addHandlers();
        $.getJSON('/api/gender', function (data) {
            _this.gender = data.gender;
            _this.drawGenderSelect();
        });

    },
    addHandlers: function() {
        var _this = this;
        let form = document.getElementById('searchForm');
        form.addEventListener("submit", ((event) => {
            event.preventDefault();
            let searchParam = this.getSearchParam();
            searchParam = searchParam ? "?" + searchParam : searchParam;
            $.getJSON('/api/contact/search' , function (data) {

            });

            // let form = document.getElementById('searchForm');
            // let formData = new FormData(form);
            // var request = new XMLHttpRequest();
            // request.open("POST", "/api/contact/search");
            // formData.append("serialnumber", serialNumber++);
            // request.send(formData);
            // $.post('/api/contact/search', formData, function (data) {
            //
            // });
        }));
    },

    getSearchParam: function() {
        let searchParam = "";
        let inputFirstName = document.getElementById('inputFirstName');
        let inputLastName = document.getElementById('inputLastName');
        let inputMiddleName = document.getElementById('inputMiddleName');
        let inputBirthDayBefore = document.getElementById('inputBirthDayBefore');
        let inputBirthDayAfter = document.getElementById('inputBirthDayAfter');
        let selectGender = document.getElementById("selectGender");
        let inputCitizenship = document.getElementById('inputCitizenship');
        let inputFamilyStatus = document.getElementById('inputFamilyStatus');
        let inputCountry = document.getElementById('inputCountry');
        let inputCity = document.getElementById('inputCity');
        let inputStreet = document.getElementById('inputStreet');
        let inputHouse = document.getElementById('inputHouse');
        let inputApartment = document.getElementById('inputApartment');
        let inputZip = document.getElementById('inputZip');

        let searchData = {};

        searchData.firstName = inputFirstName.value || null;
        searchData.lastName = inputLastName.value || null;
        searchData.middleName = inputMiddleName.value || null;
        searchData.birthDayBefore = inputBirthDayBefore.value || null;
        searchData.birthDayAfter = inputBirthDayAfter.value || null;
        searchData.gender = selectGender.value.substring(7) || null;
        searchData.citizenship = inputCitizenship.value || null;
        searchData.familyStatus = inputFamilyStatus.value || null;
        searchData.country = inputCountry.value || null;
        searchData.city = inputCity.value || null;
        searchData.street = inputStreet.value || null;
        searchData.house = inputHouse.value || null;
        searchData.apartment = inputApartment.value || null;
        searchData.postcode = inputZip.value || null;

        Object.keys(searchData).map((key) => {
            if(searchData[key]){
                searchParam += key +"="+ searchData[key] + "&";
            }
        });
        searchParam = searchParam ? searchParam.slice(0, -1) : searchParam;
        return searchParam;
    },

    drawGenderSelect: function() {
        let select = document.getElementById("selectGender");
        this.gender.map((gender) => {
            let option = document.createElement("option");
            option.appendChild(document.createTextNode(gender.name))
            option.value="gender-"+gender.id;
            select.appendChild(option)
        })
    }


};

jQuery(document).ready(function ($) {
    SearchController.execute();
});