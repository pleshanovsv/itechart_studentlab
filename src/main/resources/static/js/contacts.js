/* */

var ContactController = {
    offset: 0,
    limit: 5,

    draw: function (data) {
        let tHeadArray = ["Name", "Birth Day", "Address", "Company"];
        let container = document.querySelector('div.content');
        container.innerHTML = "";
        let table = document.createElement("table");
        let thead = document.createElement("thead");
        let trThead = document.createElement("tr");
        tHeadArray.map((str) => {
            let th = document.createElement("th");
            th.appendChild(document.createTextNode(str));
            trThead.appendChild(th);
        });
        thead.appendChild(trThead);
        table.appendChild(thead);
        container.appendChild(table);

        table.className = "table table-striped";

        let tbody = document.createElement("tbody");
        data.map((item) => this.drawContacts(item, tbody));
        table.appendChild(tbody);
    },

    drawContacts: function (contact, tbody) {
        let tr = document.createElement("tr");
        let tdName = document.createElement("td");
        let aLink = document.createElement("A");
        let name = document.createTextNode((contact.firstName || "") + " " + (contact.middleName || "") + " " + (contact.lastName || ""));
        aLink.setAttribute("href", "/edit?id=" + contact.id);
        aLink.appendChild(name);
        tdName.appendChild(aLink);
        tr.appendChild(tdName);
        let tdBirthDay = document.createElement("td");
        tdBirthDay.appendChild(document.createTextNode(contact.birthDay));
        tr.appendChild(tdBirthDay);
        let tdAddress = document.createElement("td");
        let address = contact.address;
        Object.keys(address).map(key => {
            address[key] = address[key] || "";
        });
        let adressStr = address.house + " " + address.apartment + " " + address.street + ", " + address.city + ", " + address.country + ", " + address.postcode;
        tdAddress.appendChild(document.createTextNode(adressStr));
        tr.appendChild(tdAddress);
        let tdCompany = document.createElement("td");
        tdCompany.appendChild(document.createTextNode(contact.company || ""));
        tr.appendChild(tdCompany);
        tbody.appendChild(tr);
    },

    execute: function () {
        var _this = this;
        $.getJSON('/api/contact/list?limit=' + _this.limit + '&offset=' + _this.offset, function (data) {
            _this.draw(data.contacts);
        });
    }
};

// jQuery(document).ready(function ($) {
//     ContactController.execute();
// });
