/* */

var EditorController = {
    phoneCounter: 0,
    gender: [],
    phoneType: [],
    personPhoto: "media/person.jpg",
    contactPhoto: {},

    execute: function() {
        var _this = this;
        var query = window.location.search.substring(1);
        var params = query.split("&");
        var id = params[0].split("=")[1];
        this.addHandlers();
        $.getJSON('/api/gender', function (data) {
            _this.gender = data.gender;
            _this.drawGenderSelect();
        });
        $.getJSON('/api/phonetype', function (data) {
            _this.phoneType = data.phoneType;
        });
        if(id) {
            $.getJSON('/api/contact?id=' + id, function (data) {
                _this.draw(data);
                var d=data;
            });
        }

        let imgPhoto = document.getElementById('imgPhoto');
        imgPhoto.src = this.personPhoto;
    },

    draw: function(data) {
        let inputContactId = document.getElementById('inputFirstName');
        let inputFirstName = document.getElementById('inputFirstName');
        let inputLastName = document.getElementById('inputLastName');
        let inputMiddleName = document.getElementById('inputMiddleName');
        let inputBirthDay = document.getElementById('inputBirthDay');
        let selectGender = document.getElementById("selectGender");
        let inputCitizenship = document.getElementById('inputCitizenship');
        let inputFamilyStatus = document.getElementById('inputFamilyStatus');
        let inputWebSite = document.getElementById('inputWebSite');
        let inputEmail = document.getElementById('inputEmail');
        let inputCompany = document.getElementById('inputCompany');
        let addressForm = document.getElementById('addressForm');
        let inputCountry = document.getElementById('inputCountry');
        let inputCity = document.getElementById('inputCity');
        let inputStreet = document.getElementById('inputStreet');
        let inputHouse = document.getElementById('inputHouse');
        let inputApartment = document.getElementById('inputApartment');
        let inputZip = document.getElementById('inputZip');

        if(data.id) {
            inputContactId.value = data.id;
        }
        inputFirstName.value = data.firstName || "";
        inputLastName.value = data.lastName || "";
        inputMiddleName.value = data.middleName || "";
        inputBirthDay.value = data.birthDay || "";
        selectGender.value = "gender-"+data.gender.id;
        inputCitizenship.value = data.citizenship || "";
        inputFamilyStatus.value = data.familyStatus || "";
        inputWebSite.value = data.webSite || "";
        inputEmail.value = data.email || "";
        inputCompany.value = data.company || "";
        if(data.address.id) {
            addressForm.dataset.addressId = data.address.id;
        }
        inputCountry.value = data.address.country || "";
        inputCity.value = data.address.city || "";
        inputStreet.value = data.address.street || "";
        inputHouse.value = data.address.house || "";
        inputApartment.value = data.address.apartment || "";
        inputZip.value = data.address.postcode || "";

        let tbodyPhone = document.getElementById('tbody-phone');
        data.phoneList.map((phone) => {
            this.drawPhoneRow(tbodyPhone, phone);
            this.phoneCounter++;
        });

    },

    addHandlers: function() {
        let form = document.getElementById('editForm');
        // form.addEventListener("submit", ((event) => {
        //     event.preventDefault();
        //     let data = this.getFormData();
        //     $.ajax ({
        //         type: "POST",
        //         url: "/api/contact",
        //         data: JSON.stringify(data),
        //         dataType: "json",
        //         contentType: "application/json; charset=utf-8",
        //         success: function(d){
        //             console.log(d);
        //         }
        //     });
        // }));

        let inputPhoto = document.getElementById('inputPhoto');
        inputPhoto.addEventListener("change", inputPhotoHandler);
        function inputPhotoHandler() {
            if (this.files && this.files[0]) {
                var reader = new FileReader();
                reader.onload = function (event) {
                    let imgPhoto = document.getElementById('imgPhoto');
                    imgPhoto.src = event.target.result;
                };
                reader.readAsDataURL(this.files[0]);
            }
        };

        let btnPhoneAdd = document.getElementById('btn-phone-add');
        let tbodyPhone = document.getElementById('tbody-phone');
        btnPhoneAdd.addEventListener("click", ((event) => {
            event.preventDefault();
            this.drawPhoneRow(tbodyPhone);
            this.phoneCounter++;
        }))
    },

    getFormData: function() {
        let data = {};

        let form = document.getElementById('editForm');
        let inputFirstName = document.getElementById('inputFirstName');
        let inputLastName = document.getElementById('inputLastName');
        let inputMiddleName = document.getElementById('inputMiddleName');
        let inputBirthDay = document.getElementById('inputBirthDay');
        let selectGender = document.getElementById("selectGender");
        let inputCitizenship = document.getElementById('inputCitizenship');
        let inputFamilyStatus = document.getElementById('inputFamilyStatus');
        let inputWebSite = document.getElementById('inputWebSite');
        let inputEmail = document.getElementById('inputEmail');
        let inputCompany = document.getElementById('inputCompany');
        let addressForm = document.getElementById('addressForm');
        let inputCountry = document.getElementById('inputCountry');
        let inputCity = document.getElementById('inputCity');
        let inputStreet = document.getElementById('inputStreet');
        let inputHouse = document.getElementById('inputHouse');
        let inputApartment = document.getElementById('inputApartment');
        let inputZip = document.getElementById('inputZip');

        if(form.dataset.contactId) {
            data.id = form.dataset.contactId;
        }
        data.firstName = inputFirstName.value || null;
        data.lastName = inputLastName.value || null;
        data.middleName = inputMiddleName.value || null;
        data.birthDay = inputBirthDay.value || null;
        data.gender = {};
        data.gender.id = selectGender.value.substring(7);
        data.citizenship = inputCitizenship.value || null;
        data.familyStatus = inputFamilyStatus.value || null;
        data.webSite = inputWebSite.value || null;
        data.email = inputEmail.value || null;
        data.company = inputCompany.value || null;
        data.address = {};
        data.address.country = inputCountry.value || null;
        data.address.city = inputCity.value || null;
        data.address.street = inputStreet.value || null;
        data.address.house = inputHouse.value || null;
        data.address.apartment = inputApartment.value || null;
        data.address.postcode = inputZip.value || null;

        let isAddressEmpty = Object.keys(data.address).every(key => data.address[key] == null);
        if(isAddressEmpty) {
            data.address = null;
        }
        if(data.address && addressForm.dataset.addressId) {
            data.address.id = addressForm.dataset.addressId;
        }

        data.phoneList = null;

        let tbodyPhone = document.getElementById('tbody-phone');
        if(tbodyPhone.getElementsByTagName("tr")){
            data.phoneList = [];
            let trPhoneList = tbodyPhone.getElementsByTagName("tr");
            let i;
            for (i = 0; i < trPhoneList.length; i++) {
                let trPhoneListChildren = trPhoneList[i].children;
                data.phoneList[i] = {};
                data.phoneList[i].countryCode = trPhoneListChildren[0].getElementsByTagName("input")[0].value || null;
                data.phoneList[i].operatorCode = trPhoneListChildren[1].getElementsByTagName("input")[0].value || null;
                data.phoneList[i].phone = trPhoneListChildren[2].getElementsByTagName("input")[0].value || null;
                data.phoneList[i].comment = trPhoneListChildren[4].getElementsByTagName("input")[0].value || null;

                let isPhoneListEmpty = Object.keys(data.phoneList[i]).every(key => data.phoneList[i][key] == null);
                if(isPhoneListEmpty) {
                    data.phoneList[i] = null;
                } else {
                    if(data.phoneList[i] && trPhoneList[i].dataset.phoneId) {
                        data.phoneList[i].id = trPhoneList[i].dataset.phoneId;
                    }
                    data.phoneList[i].phoneType = {};
                    data.phoneList[i].phoneType.id = trPhoneListChildren[3].getElementsByTagName("select")[0].value.substring(11);
                }
            }
            data.phoneList = data.phoneList.filter( phone => phone !== null);
            if (data.phoneList.length == 0) {
                data.phoneList = null;
            }
        }
        return data;
    },

    drawPhoneRow: function(tbody, phone) {
        let tr = document.createElement("tr");
        if(phone && phone.id){
            tr.dataset.phoneId = phone.id;
        }
        let tdCountryCode = document.createElement("td");
        let tdOperatorCode = document.createElement("td");
        let tdPhone = document.createElement("td");
        let tdPhoneType = document.createElement("td");
        let tdComment = document.createElement("td");
        let tdBtnRemove = document.createElement("td");

        let inputCountryCode = document.createElement("INPUT");
        inputCountryCode.className = "form-control";
        inputCountryCode.name = "countryCode-"+this.phoneCounter;
        inputCountryCode.setAttribute("type", "text");
        inputCountryCode.setAttribute("placeholder", "Enter country code");
        tdCountryCode.appendChild(inputCountryCode);
        tr.appendChild(tdCountryCode);

        let inputOperatorCode = document.createElement("INPUT");
        inputOperatorCode.className = "form-control";
        inputOperatorCode.name = "operatorCode-"+this.phoneCounter;
        inputOperatorCode.setAttribute("type", "text");
        inputOperatorCode.setAttribute("placeholder", "Enter operator code");
        tdOperatorCode.appendChild(inputOperatorCode);
        tr.appendChild(tdOperatorCode);

        let inputPhone = document.createElement("INPUT");
        inputPhone.className = "form-control";
        inputPhone.name = "phone-"+this.phoneCounter;
        inputPhone.setAttribute("type", "text");
        inputPhone.setAttribute("placeholder", "Enter phone");
        tdPhone.appendChild(inputPhone);
        tr.appendChild(tdPhone);

        let selectPhoneType = document.createElement("select");
        selectPhoneType.className = "form-control";
        selectPhoneType.name = "phoneType-"+this.phoneCounter;
        this.phoneType.map((phoneType) => {
            let option = document.createElement("option");
            option.appendChild(document.createTextNode(phoneType.name))
            option.value="phone-type-"+phoneType.id;
            selectPhoneType.appendChild(option);
        });
        tdPhoneType.appendChild(selectPhoneType);
        tr.appendChild(tdPhoneType);

        let inputComment = document.createElement("INPUT");
        inputComment.className = "form-control";
        inputComment.name = "comment-"+this.phoneCounter;
        inputComment.setAttribute("type", "text");
        inputComment.setAttribute("placeholder", "Enter comment");
        tdComment.appendChild(inputComment);
        tr.appendChild(tdComment);

        let btnRemove = document.createElement("button");
        let spanRemove = document.createElement("span");
        btnRemove.className = "btn btn-remove btn-danger";
        // btnRemove.id = "btn-phone-"+this.phoneCounter;
        spanRemove.className = "glyphicon glyphicon-minus";
        btnRemove.appendChild(spanRemove);
        tdBtnRemove.appendChild(btnRemove);
        tr.appendChild(tdBtnRemove);

        tbody.appendChild(tr);

        if(phone) {
            inputCountryCode.value = phone.countryCode || "";
            inputOperatorCode.value = phone.operatorCode || "";
            inputPhone.value = phone.phone || "";
            inputComment.value = phone.comment || "";
            selectPhoneType.value = "phone-type-" + phone.phoneType.id;
        }

        btnRemove.onclick = function(event) {
            event.preventDefault();
            let btn = event.currentTarget;
            let tdPhone = btn.parentNode;
            let trPhone = tdPhone.parentNode;
            trPhone.remove();
        }
    },

    drawGenderSelect: function() {
        let select = document.getElementById("selectGender");
        this.gender.map((gender) => {
            let option = document.createElement("option");
            option.appendChild(document.createTextNode(gender.name))
            option.value="gender-"+gender.id;
            select.appendChild(option)
        })
    }


};

jQuery(document).ready(function ($) {
    EditorController.execute(3);
});