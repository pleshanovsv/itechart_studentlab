package by.itechart.contactlist.service;

import by.itechart.contactlist.dao.interfaces.PhoneDao;
import by.itechart.contactlist.dao.PhoneDaoImpl;
import by.itechart.contactlist.model.Phone;
import by.itechart.contactlist.model.PhoneType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class PhoneService implements Service {
    private static PhoneService ourInstance = new PhoneService();

    private final Logger log = LoggerFactory.getLogger(PhoneService.class);

    private PhoneTypeService phoneTypeService = PhoneTypeService.getInstance();

    private PhoneDao phoneDao = new PhoneDaoImpl();

    private PhoneService() {
    }

    public static PhoneService getInstance() {
        return ourInstance;
    }

    public Integer save(Phone phone) {
        Integer id = phoneDao.save(phone);
        return id;
    }

    public void saveAll(List<Phone> phones) {
        if (phones == null) {
            return ;
        }
        for (Phone phone : phones) {
            save(phone);
        }
    }

    public List<Phone> getByContactId(Integer contactId) {
        List<Phone> phoneList = phoneDao.getByContactId(contactId);
        phoneList.forEach(phone -> {
            PhoneType phoneType = phone.getPhoneType();
            phoneType = phoneTypeService.get(phoneType.getId());
            phone.setPhoneType(phoneType);
        });
        return phoneList;
    }

    public void deleteByContactId(Integer id){
        phoneDao.deleteByContactId(id);
    }

}
