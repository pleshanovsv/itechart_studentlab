package by.itechart.contactlist.service;

import by.itechart.contactlist.dao.interfaces.AddressDao;
import by.itechart.contactlist.dao.AddressDaoImpl;
import by.itechart.contactlist.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AddressService implements Service {
    private static AddressService ourInstance = new AddressService();

    private final Logger log = LoggerFactory.getLogger(AddressService.class);
    private AddressDao addressDao = new AddressDaoImpl();

    private AddressService() {
    }

    public static AddressService getInstance() {
        return ourInstance;
    }

    public Integer save(Address address) {
        Integer id = addressDao.save(address);
        return id;
    }

    public Address getByContactId(Integer contactId) {
        Address address = addressDao.getByContactId(contactId);
        return address;
    }

    public void deleteByContactId(Integer id){
        addressDao.deleteByContactId(id);
    }
}
