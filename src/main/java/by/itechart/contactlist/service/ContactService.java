package by.itechart.contactlist.service;

import by.itechart.contactlist.dao.ContactDaoImpl;
import by.itechart.contactlist.dao.interfaces.ContactDao;
import by.itechart.contactlist.model.*;
import by.itechart.contactlist.service.dto.ContactInfo;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class ContactService implements Service {
    private static ContactService ourInstance = new ContactService();

    private final Logger log = LoggerFactory.getLogger(ContactService.class);

    private GenderService genderService = GenderService.getInstance();
    private AddressService addressService = AddressService.getInstance();
    private PhoneService phoneService = PhoneService.getInstance();
    private AttachmentService attachmentService = AttachmentService.getInstance();

    private ContactDao contactDao = new ContactDaoImpl();

    {
        if (contactDao.isEmpty()) {
            importFromJson();
        }
    }

    private ContactService() {
    }

    public static ContactService getInstance() {
        return ourInstance;
    }

    public Contact get(Integer id) {
        Contact contact = contactDao.get(id);
        Integer contactId = contact.getId();
        Integer genderId = contact.getGender().getId();
        Gender gender = genderService.get(genderId);
        Address address = addressService.getByContactId(contactId);
        List<Phone> phoneList = phoneService.getByContactId(contactId);
        contact.setGender(gender);
        contact.setAddress(address);
        contact.setPhoneList(phoneList);
        return contact;
    }

    public List<ContactInfo> list(int offset, int limit) {
        List<Contact> contactList = contactDao.list(offset, limit);
        List<ContactInfo> contactInfoList = contactList.stream().map(contact -> {
            Address address = addressService.getByContactId(contact.getId());
            ContactInfo contactInfo = new ContactInfo();
            contactInfo.setFirstName(contact.getFirstName());
            contactInfo.setLastName(contact.getLastName());
            contactInfo.setMiddleName(contact.getMiddleName());
            contactInfo.setId(contact.getId());
            contactInfo.setBirthDay(contact.getBirthDay());
            contactInfo.setCompany(contact.getCompany());
            contactInfo.setAddress(address);
            return contactInfo;
        }).collect(Collectors.toList());
        return contactInfoList;
    }

    public Integer getCount() {
        Integer count = contactDao.getAll().size();
        return count;
    }

    public Integer save(Contact contact) {
        Integer id = contactDao.save(contact);
        if(Objects.nonNull(id)) {
            addressService.deleteByContactId(id);
            Address address = contact.getAddress();
            if (Objects.nonNull(address)) {
                address.setContactId(id);
                addressService.save(address);
            }
            phoneService.deleteByContactId(id);
            List<Phone> phoneList = contact.getPhoneList();
            if (Objects.nonNull(phoneList)) {
                phoneList.forEach(phone -> phone.setContactId(id));
                phoneService.saveAll(phoneList);
            }
            attachmentService.deleteByContactId(id);
            List<Attachment> attachments = contact.getAttachments();
            if (Objects.nonNull(attachments)) {
                attachments.forEach(attachment -> attachment.setContactId(id));
                attachmentService.saveAll(attachments);
            }
        }
        return id;
    }

    public void saveAll(List<Contact> contacts) {
        if (contacts == null) {
            return ;
        }
        for (Contact contact : contacts) {
            save(contact);
        }
    }

    private void importFromJson() {
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonFactory jsonFactory = new JsonFactory();
            InputStream is = getClass().getResourceAsStream("/contacts.json");
            JsonParser jsonParser = jsonFactory.createParser(is);
            JavaType collectionType = mapper.getTypeFactory().constructCollectionType(List.class, Contact.class);
            List<Contact> contactList = mapper.readValue(jsonParser, collectionType);
            saveAll(contactList);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
