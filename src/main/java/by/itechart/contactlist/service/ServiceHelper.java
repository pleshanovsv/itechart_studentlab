package by.itechart.contactlist.service;

import java.util.HashMap;

public class ServiceHelper {
    private static ServiceHelper ourInstance = new ServiceHelper();
    private HashMap<String, Service> services = new HashMap<>();

    private ServiceHelper() {
        services.put("genderService", GenderService.getInstance());
        services.put("phoneTypeService", PhoneTypeService.getInstance());
        services.put("addressService", AddressService.getInstance());
        services.put("phoneService", PhoneService.getInstance());
        services.put("attachmentService", AttachmentService.getInstance());
        services.put("contactService", ContactService.getInstance());
    }

    public static ServiceHelper getInstance() {
        return ourInstance;
    }

    public Service get(String serviceName) {
        Service service = services.get(serviceName);
        return service;
    }

}
