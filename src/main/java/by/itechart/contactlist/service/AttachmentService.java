package by.itechart.contactlist.service;

import by.itechart.contactlist.dao.interfaces.AttachmentDao;
import by.itechart.contactlist.dao.AttachmentDaoImpl;
import by.itechart.contactlist.model.Attachment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class AttachmentService implements Service {
    private static AttachmentService ourInstance = new AttachmentService();

    private final Logger log = LoggerFactory.getLogger(AttachmentService.class);
    private AttachmentDao attachmentDao = new AttachmentDaoImpl();

    private AttachmentService() {
    }

    public static AttachmentService getInstance() {
        return ourInstance;
    }

    public Integer save(Attachment attachment) {
        Integer id = attachmentDao.save(attachment);
        return id;
    }

    public void saveAll(List<Attachment> attachments) {
        if (attachments == null) {
            return ;
        }
        for (Attachment attachment : attachments) {
            save(attachment);
        }
    }

    public void deleteByContactId(Integer id){
        attachmentDao.deleteByContactId(id);
    }

}
