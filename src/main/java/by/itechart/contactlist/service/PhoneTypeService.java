package by.itechart.contactlist.service;

import by.itechart.contactlist.dao.interfaces.PhoneTypeDao;
import by.itechart.contactlist.dao.PhoneTypeDaoImpl;
import by.itechart.contactlist.model.PhoneType;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class PhoneTypeService implements Service {
    private static PhoneTypeService ourInstance = new PhoneTypeService();

    private final Logger log = LoggerFactory.getLogger(PhoneTypeService.class);
    private PhoneTypeDao phoneTypeDao = new PhoneTypeDaoImpl();

    {
        if (phoneTypeDao.isEmpty()) {
            importFromJson();
        }
    }

    private PhoneTypeService() {
    }

    public static PhoneTypeService getInstance() {
        return ourInstance;
    }

    public List<PhoneType> getAll(){
        List<PhoneType> phoneTypeList = phoneTypeDao.getAll();
        return phoneTypeList;
    }

    public PhoneType get(Integer id){
        PhoneType phoneType = phoneTypeDao.get(id);
        return phoneType;
    }

    private void importFromJson() {
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonFactory jsonFactory = new JsonFactory();
            InputStream is = getClass().getResourceAsStream("/phone_type.json");
            JsonParser jsonParser = jsonFactory.createParser(is);
            JavaType collectionType = mapper.getTypeFactory().constructCollectionType(List.class, PhoneType.class);
            List<PhoneType> phoneTypeList = mapper.readValue(jsonParser, collectionType);
            phoneTypeDao.saveAll(phoneTypeList);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
