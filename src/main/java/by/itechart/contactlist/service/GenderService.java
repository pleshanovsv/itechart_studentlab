package by.itechart.contactlist.service;

import by.itechart.contactlist.dao.interfaces.GenderDao;
import by.itechart.contactlist.dao.GenderDaoImpl;
import by.itechart.contactlist.model.Gender;
import by.itechart.contactlist.model.PhoneType;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class GenderService implements Service {
    private static GenderService ourInstance = new GenderService();

    private final Logger log = LoggerFactory.getLogger(GenderService.class);
    private GenderDao genderDao = new GenderDaoImpl();

    {
        if (genderDao.isEmpty()) {
            importFromJson();
        }
    }

    private GenderService() {
    }

    public static GenderService getInstance() {
        return ourInstance;
    }

    public Gender get(Integer id) {
        Gender gender = genderDao.get(id);
        return gender;
    }

    public List<Gender> getAll(){
        List<Gender> genderList = genderDao.getAll();
        return genderList;
    }

    private void importFromJson() {
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonFactory jsonFactory = new JsonFactory();
            InputStream is = getClass().getResourceAsStream("/gender.json");
            JsonParser jsonParser = jsonFactory.createParser(is);
            JavaType collectionType = mapper.getTypeFactory().constructCollectionType(List.class, Gender.class);
            List<Gender> genderList = mapper.readValue(jsonParser, collectionType);
            genderDao.saveAll(genderList);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
