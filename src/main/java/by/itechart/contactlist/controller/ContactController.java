package by.itechart.contactlist.controller;

import by.itechart.contactlist.model.Contact;
import by.itechart.contactlist.service.ContactService;
import by.itechart.contactlist.service.dto.ContactInfo;
import com.sun.deploy.net.HttpResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpRequest;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class ContactController {

    private final Logger log = LoggerFactory.getLogger(ContactController.class);

    private ContactService contactService = ContactService.getInstance();

    @RequestMapping("/api/contact/list")
    public Map<String, Object> list(@RequestParam(value="limit", defaultValue="10") Integer limit,@RequestParam(value="offset", defaultValue="0") Integer offset) {
        log.debug("contacts list");
        Map<String, Object> result = new HashMap<>();
        List<ContactInfo> contactInfoList = contactService.list(offset, limit);
        result.put("contacts", contactInfoList);
        result.put("total", contactService.getCount());
        return result;
    }

    @RequestMapping(value = "/api/contact", method = RequestMethod.GET)
    public Contact getContact(@RequestParam(value="id" ) Integer id) {
        log.debug("get contact " + id);
        Contact contact = contactService.get(id);
        return contact;
    }

//    @RequestMapping(value = "/api/contact", method = RequestMethod.POST)
//    public void saveContact(HttpRequest httpRequest, HttpResponse httpResponse) {
//        log.debug("save contact ");
////        Integer id = contactService.save(contact);
////        return id;
//    }

    @RequestMapping(value = "/api/contact/search", method = RequestMethod.GET)
    public void searchContact(
            @RequestParam(value = "firstName", defaultValue = "") String firstName,
            @RequestParam(value = "lastName", defaultValue = "") String lastName,
            @RequestParam(value = "middleName", defaultValue = "") String middleName,
            @RequestParam(value = "birthDayBefore", defaultValue = "") String birthDayBefore,
            @RequestParam(value = "birthDayAfter", defaultValue = "") String birthDayAfter,
            @RequestParam(value = "gender", defaultValue = "") String gender,
            @RequestParam(value = "citizenship", defaultValue = "") String citizenship,
            @RequestParam(value = "familyStatus", defaultValue = "") String familyStatus,
            @RequestParam(value = "country", defaultValue = "") String country,
            @RequestParam(value = "city", defaultValue = "") String city,
            @RequestParam(value = "street", defaultValue = "") String street,
            @RequestParam(value = "house", defaultValue = "") String house,
            @RequestParam(value = "apartment", defaultValue = "") String apartment,
            @RequestParam(value = "postcode", defaultValue = "") String postcode
    ) {

        log.debug("save contact " + firstName);
    }

}
