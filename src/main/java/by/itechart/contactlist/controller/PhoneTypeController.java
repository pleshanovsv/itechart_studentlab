package by.itechart.contactlist.controller;

import by.itechart.contactlist.model.PhoneType;
import by.itechart.contactlist.service.PhoneTypeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class PhoneTypeController {

    private final Logger log = LoggerFactory.getLogger(PhoneTypeController.class);

    private PhoneTypeService phoneTypeService = PhoneTypeService.getInstance();

    @RequestMapping("/api/phonetype")
    public Map<String, Object> list() {
        log.debug("phoneType list");
        Map<String, Object> result = new HashMap<>();
        List<PhoneType> phoneTypeList = phoneTypeService.getAll();
        result.put("phoneType", phoneTypeList);
        return result;
    }

}
