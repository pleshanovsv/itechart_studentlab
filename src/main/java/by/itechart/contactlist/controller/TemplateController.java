package by.itechart.contactlist.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;


@Controller
public class TemplateController {

    private final Logger log = LoggerFactory.getLogger(TemplateController.class);

    @RequestMapping(value = "/api/contact", method = RequestMethod.POST)
    public void saveContact(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        log.debug("save contact {} {}", request, response);
        for (Part filePart : request.getParts()) {
            String fileName = filePart.getName();
            File repository = new File("D://Contacts");
            if (!repository.exists()) {
                repository.mkdir();
            }
            fileName = filePart.getSubmittedFileName();
            filePart.write("D://Contacts" + File.separator + fileName);
        }
        //Apache Commons
//        try {
//            if (!ServletFileUpload.isMultipartContent(request)) {
//                log.debug("Not a multipart request.");
//                return;
//            }
//            DiskFileItemFactory factory = new DiskFileItemFactory();
//            File repository = new File("D://Contacts");
//            factory.setRepository(repository);
//            // Create a new file upload handler
//            ServletFileUpload upload = new ServletFileUpload(factory);
//            upload.setFileSizeMax(1024 * 1024 * 40);
//            upload.setSizeMax(1024 * 1024 * 50);
//            // Parse the request
//            List<FileItem> items = upload.parseRequest(request);
//            FileItemIterator iter = upload.getItemIterator(request);
//            while (iter.hasNext()) {
//                FileItemStream item = iter.next();
//                String name = item.getFieldName();
//                InputStream stream = item.openStream();
//                if (!item.isFormField()) {
//                    String filename = item.getName();
//                    // Process the input stream
//                    OutputStream out = new FileOutputStream(filename);
//                    IOUtils.copy(stream, out);
//                    stream.close();
//                    out.close();
//                }
//            }
//        } catch (FileUploadException e) {
//            log.error("FileUploadException: {}", e.getMessage());
//            return;
//        } catch (IOException e) {
//            log.error("Internal server IO error: {}", e.getMessage());
//            return;
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        return;

    }

    @GetMapping("/home")
    public String index() {
        return "index.html";
    }

    @GetMapping("/search")
    public String search() {
        return "search.html";
    }

    @GetMapping("/edit")
    public String edit() {
        return "detail.html";
    }
}
