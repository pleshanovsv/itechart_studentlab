package by.itechart.contactlist.controller;

import by.itechart.contactlist.model.Gender;
import by.itechart.contactlist.service.ContactService;
import by.itechart.contactlist.service.GenderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class GenderController {

    private final Logger log = LoggerFactory.getLogger(GenderController.class);

    private GenderService genderService = GenderService.getInstance();

    @RequestMapping("/api/gender")
    public Map<String, Object> list() {
        log.debug("gender list");
        Map<String, Object> result = new HashMap<>();
        List<Gender> genderList = genderService.getAll();
        result.put("gender", genderList);
        return result;
    }

}
