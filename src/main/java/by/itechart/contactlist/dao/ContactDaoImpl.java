package by.itechart.contactlist.dao;

import by.itechart.contactlist.dao.interfaces.ContactDao;
import by.itechart.contactlist.manager.ConnectionManager;
import by.itechart.contactlist.model.Contact;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ContactDaoImpl extends AbstractDao implements ContactDao {

    private final Logger log = LoggerFactory.getLogger(ContactDaoImpl.class);
    private ConnectionManager connectionPool = ConnectionManager.getInstance();

    @Override
    public List<Contact> list(int offset, int limit) {
        List<Contact> contactList = new ArrayList<>();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            preparedStatement = connection.prepareStatement("SELECT * FROM contact LIMIT ? OFFSET ? ;");
            preparedStatement.setInt(1, limit);
            preparedStatement.setInt(2, offset);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Contact contact = mapModelFromResultSet(resultSet);
                contactList.add(contact);
            }
        } catch (SQLException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        } finally {
            closeConnection(resultSet, preparedStatement, connection);
        }
        return contactList;
    }

    @Override
    public List<Contact> getAll() {
        List<Contact> contactList = new ArrayList<>();
        Statement statement = null;
        ResultSet resultSet = null;
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT * FROM contact;");
            while (resultSet.next()) {
                Contact contact = mapModelFromResultSet(resultSet);
                contactList.add(contact);
            }
        } catch (SQLException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        } finally {
            closeConnection(resultSet, statement, connection);
        }
        return contactList;
    }

    @Override
    public Contact get(Integer id) {
        if (Objects.isNull(id)) {
            return null;
        }
        Contact contact = new Contact();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            preparedStatement = connection.prepareStatement("SELECT * FROM contact where id=?;");
            preparedStatement.setInt(1, id);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                contact = mapModelFromResultSet(resultSet);
            }
        } catch (SQLException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        } finally {
            closeConnection(resultSet, preparedStatement, connection);
        }
        return contact;
    }

    @Override
    public Integer save(Contact contact) {
        if(isNew(contact)){
            return persist(contact);
        } else {
            return update(contact);
        }
    }

    @Override
    public void saveAll(Iterable<Contact> contacts) {
        if (contacts == null) {
            return ;
        }
        for (Contact contact : contacts) {
            save(contact);
        }
    }

    @Override
    public void delete(Integer id){
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            preparedStatement = connection.prepareStatement("DELETE FROM contact where id=?;");
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        } finally {
            closeConnection(preparedStatement, connection);
        }
    }

    @Override
    public boolean isEmpty() {
        boolean isEmpty = true;
        Contact contact = new Contact();
        Statement statement = null;
        ResultSet resultSet = null;
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT * FROM contact;");
            if (resultSet.next()) {
                isEmpty = false;
            }
        } catch (SQLException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        } finally {
            closeConnection(resultSet, statement, connection);
        }
        return isEmpty;
    }

    private Integer persist(Contact contact) {
        if (Objects.isNull(contact)) {
            return null;
        }
        Integer id = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            String insertSQL = "INSERT INTO contact " +
                    "(first_name, last_name, middle_name, birth_day, gender_id, citizenship, family_status, web_site, email, company) " +
                    "VALUES(?,?,?,?,?,?,?,?,?,?);";
            preparedStatement = connection.prepareStatement(insertSQL, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, contact.getFirstName());
            preparedStatement.setString(2, contact.getLastName());
            preparedStatement.setString(3, contact.getMiddleName());
            preparedStatement.setObject(4, contact.getBirthDay());
            preparedStatement.setObject(5, contact.getGender().getId());
            preparedStatement.setString(6, contact.getCitizenship());
            preparedStatement.setString(7, contact.getFamilyStatus());
            preparedStatement.setString(8, contact.getWebSite());
            preparedStatement.setString(9, contact.getEmail());
            preparedStatement.setString(10, contact.getCompany());
            preparedStatement.executeUpdate();
            resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                int savedId = resultSet.getInt(1);
                id = new Integer(savedId);
            }
        } catch (SQLException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        } finally {
            closeConnection(resultSet, preparedStatement, connection);
        }
        return id;
    }

    private Integer update(Contact contact) {
        if(Objects.isNull(contact)){
            return null;
        }
        Integer id = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            String updateSQL = "UPDATE contact SET " +
                    "first_name=?, last_name=?, middle_name=?, birth_day=?, gender_id=?, citizenship=?, family_status=?, web_site=?, email=?, company=? " +
                    "WHERE id=?;";
            preparedStatement = connection.prepareStatement(updateSQL, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, contact.getFirstName());
            preparedStatement.setString(2, contact.getLastName());
            preparedStatement.setString(3, contact.getMiddleName());
            preparedStatement.setObject(4, contact.getBirthDay());
            preparedStatement.setObject(5, contact.getGender().getId());
            preparedStatement.setString(6, contact.getCitizenship());
            preparedStatement.setString(7, contact.getFamilyStatus());
            preparedStatement.setString(8, contact.getWebSite());
            preparedStatement.setString(9, contact.getEmail());
            preparedStatement.setString(10, contact.getCompany());
            preparedStatement.setInt(11, contact.getId());
            preparedStatement.executeUpdate();
            resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                int savedId = resultSet.getInt(1);
                id = new Integer(savedId);
            }
        } catch (SQLException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        } finally {
            closeConnection(resultSet, preparedStatement, connection);
        }
        return id;
    }

    private Contact mapModelFromResultSet(ResultSet resultSet) throws SQLException {
        Contact contact = new Contact();
        contact.setId(resultSet.getInt("id"));
        contact.setFirstName(resultSet.getString("first_name"));
        contact.setLastName(resultSet.getString("last_name"));
        contact.setMiddleName(resultSet.getString("middle_name"));
        contact.setBirthDay(resultSet.getObject("birth_day", LocalDate.class));
        contact.getGender().setId(resultSet.getInt("gender_id"));
        contact.setCitizenship(resultSet.getString("citizenship"));
        contact.setFamilyStatus(resultSet.getString("family_status"));
        contact.setWebSite(resultSet.getString("web_site"));
        contact.setEmail(resultSet.getString("email"));
        contact.setCompany(resultSet.getString("company"));
        return contact;
    }

}
