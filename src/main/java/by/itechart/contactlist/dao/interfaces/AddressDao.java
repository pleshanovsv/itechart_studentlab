package by.itechart.contactlist.dao.interfaces;

import by.itechart.contactlist.model.Address;

import java.util.List;

public interface AddressDao {
    Address get(Integer id);

    List<Address> getAll();

    Integer save(Address address);

    void saveAll(Iterable<Address> addresses);

    void delete(Integer id);

    void deleteByContactId(Integer id);

    boolean isEmpty();

    Address getByContactId(Integer contactId);
}
