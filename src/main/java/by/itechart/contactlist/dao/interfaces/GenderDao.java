package by.itechart.contactlist.dao.interfaces;

import by.itechart.contactlist.model.Gender;

import java.util.List;

public interface GenderDao {
    Gender get(Integer id);

    List<Gender> getAll();

    Integer save(Gender gender);

    void saveAll(Iterable<Gender> contacts);

    void delete(Integer id);

    boolean isEmpty();
}
