package by.itechart.contactlist.dao.interfaces;

import by.itechart.contactlist.model.Contact;

import java.util.List;

public interface ContactDao {
    Contact get(Integer id);

    List<Contact> getAll();

    List<Contact> list(int offset, int limit);

    Integer save(Contact contact);

    void saveAll(Iterable<Contact> contacts);

    void delete(Integer id);

    boolean isEmpty();
}
