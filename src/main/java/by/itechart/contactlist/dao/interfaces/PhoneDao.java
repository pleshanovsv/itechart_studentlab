package by.itechart.contactlist.dao.interfaces;

import by.itechart.contactlist.model.Phone;

import java.util.List;

public interface PhoneDao {
    Phone get(Integer id);

    List<Phone> getAll();

    Integer save(Phone phone);

    void saveAll(Iterable<Phone> phones);

    void delete(Integer id);

    void deleteByContactId(Integer id);

    boolean isEmpty();

    List<Phone> getByContactId(Integer contactId);
}
