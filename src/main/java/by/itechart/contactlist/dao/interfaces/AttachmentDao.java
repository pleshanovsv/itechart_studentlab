package by.itechart.contactlist.dao.interfaces;

import by.itechart.contactlist.model.Attachment;
import by.itechart.contactlist.model.Phone;

import java.util.List;

public interface AttachmentDao {
    Attachment get(Integer id);

    List<Attachment> getAll();

    Integer save(Attachment attachment);

    void saveAll(Iterable<Attachment> attachments);

    void delete(Integer id);

    void deleteByContactId(Integer id);

    boolean isEmpty();
}
