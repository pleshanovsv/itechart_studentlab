package by.itechart.contactlist.dao.interfaces;

import by.itechart.contactlist.model.PhoneType;

import java.util.List;

public interface PhoneTypeDao {
    PhoneType get(Integer id);

    List<PhoneType> getAll();

    Integer save(PhoneType phoneType);

    void saveAll(Iterable<PhoneType> phoneTypes);

    void delete(Integer id);

    boolean isEmpty();
}
