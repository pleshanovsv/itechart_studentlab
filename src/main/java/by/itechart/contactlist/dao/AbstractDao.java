package by.itechart.contactlist.dao;

import by.itechart.contactlist.controller.ContactController;
import by.itechart.contactlist.manager.ConnectionManager;
import by.itechart.contactlist.model.BaseEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.Objects;

public abstract class AbstractDao<T extends BaseEntity> {

    protected final Logger log = LoggerFactory.getLogger(ContactController.class);

    protected boolean isNew (T model) {
        if(Objects.isNull(model.getId())){
            return true;
        }
        boolean isNew = false;
        Statement statement = null;
        Connection connection = null;
        ResultSet resultSet = null;
        try {
            ConnectionManager connectionPool = ConnectionManager.getInstance();
            connection = connectionPool.getConnection();
            statement = connection.createStatement();
            String table = model.toString();
            resultSet = statement.executeQuery("SELECT 1 FROM " + table + " where id=" + model.getId() + ";");
            if(resultSet.next()){
                isNew = false;
            } else {
                isNew = true;
            }
        } catch (SQLException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        } finally {
            closeConnection(resultSet, statement, connection);
        }
        return isNew;
    }

    protected void closeConnection(ResultSet resultSet, Statement statement, Connection connection) {
        try {
            if (resultSet != null) {
                resultSet.close();
            }
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
        }
    }

    protected void closeConnection(Statement statement, Connection connection) {
        try {
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
        }
    }

}
