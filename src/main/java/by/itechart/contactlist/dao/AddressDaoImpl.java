package by.itechart.contactlist.dao;

import by.itechart.contactlist.dao.interfaces.AddressDao;
import by.itechart.contactlist.manager.ConnectionManager;
import by.itechart.contactlist.model.Address;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class AddressDaoImpl extends AbstractDao implements AddressDao {

    private final Logger log = LoggerFactory.getLogger(AddressDaoImpl.class);
    private ConnectionManager connectionPool = ConnectionManager.getInstance();

    @Override
    public List<Address> getAll() {
        List<Address> addressList = new ArrayList<>();
        Statement statement = null;
        ResultSet resultSet = null;
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT * FROM address;");
            while (resultSet.next()) {
                Address address = mapModelFromResultSet(resultSet);
                addressList.add(address);
            }
        } catch (SQLException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        } finally {
            closeConnection(resultSet, statement, connection);
        }
        return addressList;
    }

    @Override
    public Address get(Integer id) {
        if (Objects.isNull(id)) {
            return null;
        }
        Address address = new Address();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            preparedStatement = connection.prepareStatement("SELECT * FROM address where id=?;");
            preparedStatement.setInt(1, id);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                address = mapModelFromResultSet(resultSet);
            }
        } catch (SQLException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        } finally {
            closeConnection(resultSet, preparedStatement, connection);
        }
        return address;
    }

    @Override
    public Integer save(Address address) {
        if(isNew(address)){
            return persist(address);
        } else {
            return update(address);
        }
    }

    @Override
    public void saveAll(Iterable<Address> addresss) {
        if (addresss == null) {
            return ;
        }
        for (Address address : addresss) {
            save(address);
        }
    }

    @Override
    public void delete(Integer id){
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            preparedStatement = connection.prepareStatement("DELETE FROM address where id=?;");
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        } finally {
            closeConnection(preparedStatement, connection);
        }
    }

    @Override
    public void deleteByContactId(Integer id){
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            preparedStatement = connection.prepareStatement("DELETE FROM address where contact_id=?;");
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        } finally {
            closeConnection(preparedStatement, connection);
        }
    }

    @Override
    public boolean isEmpty() {
        boolean isEmpty = true;
        Statement statement = null;
        ResultSet resultSet = null;
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT * FROM address;");
            if (resultSet.next()) {
                isEmpty = false;
            }
        } catch (SQLException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        } finally {
            closeConnection(resultSet, statement, connection);
        }
        return isEmpty;
    }

    private Integer persist(Address address) {
        if(Objects.isNull(address)){
            return null;
        }
        Integer id = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
                String insertSQL = "INSERT INTO address (country, city, street, house, apartment, postcode, contact_id) VALUES(?,?,?,?,?,?,?);";
                preparedStatement = connection.prepareStatement(insertSQL, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, address.getCountry());
            preparedStatement.setString(2, address.getCity());
            preparedStatement.setString(3, address.getStreet());
            preparedStatement.setString(4, address.getHouse());
            preparedStatement.setString(5, address.getApartment());
            preparedStatement.setString(6, address.getPostcode());
            preparedStatement.setObject(7, address.getContactId());
            preparedStatement.executeUpdate();
            resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                int savedId = resultSet.getInt(1);
                id = new Integer(savedId);
            }
        } catch (SQLException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        } finally {
            closeConnection(resultSet, preparedStatement, connection);
        }
        return id;
    }

    private Integer update(Address address) {
        if(Objects.isNull(address)){
            return null;
        }
        Integer id = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            String updateSQL = "UPDATE address SET country=?, city=?, street=?, house=?, apartment=?, postcode=?, contact_id=? WHERE id=?;";
            preparedStatement = connection.prepareStatement(updateSQL, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, address.getCountry());
            preparedStatement.setString(2, address.getCity());
            preparedStatement.setString(3, address.getStreet());
            preparedStatement.setString(4, address.getHouse());
            preparedStatement.setString(5, address.getApartment());
            preparedStatement.setString(6, address.getPostcode());
            preparedStatement.setObject(7, address.getContactId());
            preparedStatement.setObject(8, address.getId());
            preparedStatement.executeUpdate();
            resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                int savedId = resultSet.getInt(1);
                id = new Integer(savedId);
            }
        } catch (SQLException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        } finally {
            closeConnection(resultSet, preparedStatement, connection);
        }
        return id;
    }

    @Override
    public Address getByContactId(Integer contactId) {
        if (Objects.isNull(contactId)) {
            return null;
        }
        Address address = new Address();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            preparedStatement = connection.prepareStatement("SELECT * FROM address where contact_id=?;");
            preparedStatement.setInt(1, contactId);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                address = mapModelFromResultSet(resultSet);
            }
        } catch (SQLException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        } finally {
            closeConnection(resultSet, preparedStatement, connection);
        }
        return address;
    }

    private Address mapModelFromResultSet(ResultSet resultSet) throws SQLException {
        Address address = new Address();
        address.setId(resultSet.getInt("id"));
        address.setCountry(resultSet.getString("country"));
        address.setCity(resultSet.getString("city"));
        address.setStreet(resultSet.getString("street"));
        address.setHouse(resultSet.getString("house"));
        address.setApartment(resultSet.getString("apartment"));
        address.setPostcode(resultSet.getString("postcode"));
        address.setContactId(resultSet.getInt("contact_id"));
        return address;
    }

}