package by.itechart.contactlist.dao;

import by.itechart.contactlist.dao.interfaces.PhoneTypeDao;
import by.itechart.contactlist.manager.ConnectionManager;
import by.itechart.contactlist.model.PhoneType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class PhoneTypeDaoImpl extends AbstractDao implements PhoneTypeDao {

    private final Logger log = LoggerFactory.getLogger(PhoneTypeDaoImpl.class);
    private ConnectionManager connectionPool = ConnectionManager.getInstance();

    @Override
    public List<PhoneType> getAll() {
        List<PhoneType> phoneTypeList = new ArrayList<>();
        Statement statement = null;
        ResultSet resultSet = null;
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT * FROM phone_type;");
            while (resultSet.next()) {
                PhoneType phoneType = mapModelFromResultSet(resultSet);
                phoneTypeList.add(phoneType);
            }
        } catch (SQLException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        } finally {
            closeConnection(resultSet, statement, connection);
        }
        return phoneTypeList;
    }

    @Override
    public PhoneType get(Integer id) {
        if (Objects.isNull(id)) {
            return null;
        }
        PhoneType phoneType = new PhoneType();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            preparedStatement = connection.prepareStatement("SELECT * FROM phone_type where id=?;");
            preparedStatement.setInt(1, id);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                phoneType = mapModelFromResultSet(resultSet);
            }
        } catch (SQLException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        } finally {
            closeConnection(resultSet, preparedStatement, connection);
        }
        return phoneType;
    }

    @Override
    public Integer save(PhoneType phoneType) {
        if(isNew(phoneType)){
            return persist(phoneType);
        } else {
            return update(phoneType);
        }
    }

    @Override
    public void saveAll(Iterable<PhoneType> phoneTypes) {
        if (phoneTypes == null) {
            return ;
        }
        for (PhoneType phoneType : phoneTypes) {
            save(phoneType);
        }
    }

    @Override
    public void delete(Integer id){
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            preparedStatement = connection.prepareStatement("DELETE FROM phone_type where id=?;");
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        } finally {
            closeConnection(preparedStatement, connection);
        }
    }

    @Override
    public boolean isEmpty() {
        boolean isEmpty = true;
        Statement statement = null;
        ResultSet resultSet = null;
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT * FROM phone_type;");
            if (resultSet.next()) {
                isEmpty = false;
            }
        } catch (SQLException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        } finally {
            closeConnection(resultSet, statement, connection);
        }
        return isEmpty;
    }

    private Integer persist(PhoneType phoneType) {
        if(Objects.isNull(phoneType)){
            return null;
        }
        Integer id = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
                String insertSQL = "INSERT INTO phone_type (id, name) VALUES(?,?);";
                preparedStatement = connection.prepareStatement(insertSQL, Statement.RETURN_GENERATED_KEYS);
                preparedStatement.setInt(1, phoneType.getId());
                preparedStatement.setString(2, phoneType.getName());
            preparedStatement.executeUpdate();
            resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                int savedId = resultSet.getInt(1);
                id = new Integer(savedId);
            }
        } catch (SQLException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        } finally {
            closeConnection(resultSet, preparedStatement, connection);
        }
        return id;
    }

    private Integer update(PhoneType phoneType) {
        if(Objects.isNull(phoneType)){
            return null;
        }
        Integer id = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            String updateSQL = "UPDATE phone_type SET name=? WHERE id=?;";
            preparedStatement = connection.prepareStatement(updateSQL, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, phoneType.getName());
            preparedStatement.setInt(2, phoneType.getId());
            preparedStatement.executeUpdate();
            resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                int savedId = resultSet.getInt(1);
                id = new Integer(savedId);
            }
        } catch (SQLException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        } finally {
            closeConnection(resultSet, preparedStatement, connection);
        }
        return id;
    }

    private PhoneType mapModelFromResultSet(ResultSet resultSet) throws SQLException {
        PhoneType phoneType = new PhoneType();
        phoneType.setId(resultSet.getInt("id"));
        phoneType.setName(resultSet.getString("name"));
        return phoneType;
    }

}
