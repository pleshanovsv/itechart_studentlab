package by.itechart.contactlist.dao;

import by.itechart.contactlist.dao.interfaces.GenderDao;
import by.itechart.contactlist.manager.ConnectionManager;
import by.itechart.contactlist.model.Gender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class GenderDaoImpl extends AbstractDao implements GenderDao {

    private final Logger log = LoggerFactory.getLogger(GenderDaoImpl.class);
    private ConnectionManager connectionPool = ConnectionManager.getInstance();

    @Override
    public List<Gender> getAll() {
        List<Gender> genderList = new ArrayList<>();
        Statement statement = null;
        ResultSet resultSet = null;
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT * FROM gender;");
            while (resultSet.next()) {
                Gender gender = mapModelFromResultSet(resultSet);
                genderList.add(gender);
            }
        } catch (SQLException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        } finally {
            closeConnection(resultSet, statement, connection);
        }
        return genderList;
    }

    @Override
    public Gender get(Integer id) {
        if (Objects.isNull(id)) {
            return null;
        }
        Gender gender = new Gender();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            preparedStatement = connection.prepareStatement("SELECT * FROM gender where id=?;");
            preparedStatement.setInt(1, id);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                gender = mapModelFromResultSet(resultSet);
            }
        } catch (SQLException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        } finally {
            closeConnection(resultSet, preparedStatement, connection);
        }
        return gender;
    }

    @Override
    public Integer save(Gender gender) {
        if(isNew(gender)){
            return persist(gender);
        } else {
            return update(gender);
        }
    }

    @Override
    public void saveAll(Iterable<Gender> genders) {
        if (genders == null) {
            return ;
        }
        for (Gender gender : genders) {
            save(gender);
        }
    }

    @Override
    public void delete(Integer id){
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            preparedStatement = connection.prepareStatement("DELETE FROM gender where id=?;");
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        } finally {
            closeConnection(preparedStatement, connection);
        }
    }

    @Override
    public boolean isEmpty() {
        boolean isEmpty = true;
        Statement statement = null;
        ResultSet resultSet = null;
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT * FROM gender;");
            if (resultSet.next()) {
                isEmpty = false;
            }
        } catch (SQLException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        } finally {
            closeConnection(resultSet, statement, connection);
        }
        return isEmpty;
    }

    private Integer persist(Gender gender) {
        if(Objects.isNull(gender)){
            return null;
        }
        Integer id = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
                String insertSQL = "INSERT INTO gender (id, name) VALUES(?,?);";
                preparedStatement = connection.prepareStatement(insertSQL, Statement.RETURN_GENERATED_KEYS);
                preparedStatement.setInt(1, gender.getId());
                preparedStatement.setString(2, gender.getName());
            preparedStatement.executeUpdate();
            resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                int savedId = resultSet.getInt(1);
                id = new Integer(savedId);
            }
        } catch (SQLException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        } finally {
            closeConnection(resultSet, preparedStatement, connection);
        }
        return id;
    }

    private Integer update(Gender gender) {
        if(Objects.isNull(gender)){
            return null;
        }
        Integer id = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            String updateSQL = "UPDATE gender SET name=? WHERE id=?;";
            preparedStatement = connection.prepareStatement(updateSQL, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, gender.getName());
            preparedStatement.setInt(2, gender.getId());
            preparedStatement.executeUpdate();
            resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                int savedId = resultSet.getInt(1);
                id = new Integer(savedId);
            }
        } catch (SQLException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        } finally {
            closeConnection(resultSet, preparedStatement, connection);
        }
        return id;
    }

    private Gender mapModelFromResultSet(ResultSet resultSet) throws SQLException {
        Gender gender = new Gender();
        gender.setId(resultSet.getInt("id"));
        gender.setName(resultSet.getString("name"));
        return gender;
    }

}
