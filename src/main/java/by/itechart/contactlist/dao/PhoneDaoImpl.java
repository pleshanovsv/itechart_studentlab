package by.itechart.contactlist.dao;

import by.itechart.contactlist.dao.interfaces.PhoneDao;
import by.itechart.contactlist.manager.ConnectionManager;
import by.itechart.contactlist.model.Phone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class PhoneDaoImpl extends AbstractDao implements PhoneDao {

    private final Logger log = LoggerFactory.getLogger(PhoneDaoImpl.class);
    private ConnectionManager connectionPool = ConnectionManager.getInstance();

    @Override
    public List<Phone> getAll() {
        List<Phone> phoneList = new ArrayList<>();
        Statement statement = null;
        ResultSet resultSet = null;
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT * FROM phone;");
            while (resultSet.next()) {
                Phone phone = mapModelFromResultSet(resultSet);
                phoneList.add(phone);
            }
        } catch (SQLException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        } finally {
            closeConnection(resultSet, statement, connection);
        }
        return phoneList;
    }

    @Override
    public Phone get(Integer id) {
        if (Objects.isNull(id)) {
            return null;
        }
        Phone phone = new Phone();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            preparedStatement = connection.prepareStatement("SELECT * FROM phone where id=?;");
            preparedStatement.setInt(1, id);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                phone = mapModelFromResultSet(resultSet);
            }
        } catch (SQLException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        } finally {
            closeConnection(resultSet, preparedStatement, connection);
        }
        return phone;
    }

    @Override
    public Integer save(Phone phone) {
        if(isNew(phone)){
            return persist(phone);
        } else {
            return update(phone);
        }
    }

    @Override
    public void saveAll(Iterable<Phone> phones) {
        if (phones == null) {
            return ;
        }
        for (Phone phone : phones) {
            save(phone);
        }
    }

    @Override
    public void delete(Integer id){
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            preparedStatement = connection.prepareStatement("DELETE FROM phone where id=?;");
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        } finally {
            closeConnection(preparedStatement, connection);
        }
    }

    @Override
    public void deleteByContactId(Integer id){
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            preparedStatement = connection.prepareStatement("DELETE FROM phone where contact_id=?;");
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        } finally {
            closeConnection(preparedStatement, connection);
        }
    }

    @Override
    public boolean isEmpty() {
        boolean isEmpty = true;
        Statement statement = null;
        ResultSet resultSet = null;
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT * FROM phone;");
            if (resultSet.next()) {
                isEmpty = false;
            }
        } catch (SQLException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        } finally {
            closeConnection(resultSet, statement, connection);
        }
        return isEmpty;
    }

    @Override
    public List<Phone> getByContactId(Integer contactId) {
        if (Objects.isNull(contactId)) {
            return null;
        }
        List<Phone> phoneList = new ArrayList<>();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            preparedStatement = connection.prepareStatement("SELECT * FROM phone where contact_id=?;");
            preparedStatement.setInt(1, contactId);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Phone phone = mapModelFromResultSet(resultSet);
                phoneList.add(phone);
            }
        } catch (SQLException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        } finally {
            closeConnection(resultSet, preparedStatement, connection);
        }
        return phoneList;
    }

    private Integer persist(Phone phone) {
        if(Objects.isNull(phone)){
            return null;
        }
        Integer id = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
                String insertSQL = "INSERT INTO phone (country_code, operator_code, phone, phone_type_id, comment, contact_id) VALUES(?,?,?,?,?,?);";
                preparedStatement = connection.prepareStatement(insertSQL, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, phone.getCountryCode());
            preparedStatement.setString(2, phone.getOperatorCode());
            preparedStatement.setString(3, phone.getPhone());
            preparedStatement.setObject(4, phone.getPhoneType().getId());
            preparedStatement.setString(5, phone.getComment());
            preparedStatement.setObject(6, phone.getContactId());
            preparedStatement.executeUpdate();
            resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                int savedId = resultSet.getInt(1);
                id = new Integer(savedId);
            }
        } catch (SQLException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        } finally {
            closeConnection(resultSet, preparedStatement, connection);
        }
        return id;
    }

    private Integer update(Phone phone) {
        if(Objects.isNull(phone)){
            return null;
        }
        Integer id = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            String updateSQL = "UPDATE phone SET country_code=?, operator_code=?, phone=?, phone_type_id=?, comment=?, contact_id=? WHERE id=?;";
            preparedStatement = connection.prepareStatement(updateSQL, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, phone.getCountryCode());
            preparedStatement.setString(2, phone.getOperatorCode());
            preparedStatement.setString(3, phone.getPhone());
            preparedStatement.setObject(4, phone.getPhoneType().getId());
            preparedStatement.setString(5, phone.getComment());
            preparedStatement.setObject(6, phone.getContactId());
            preparedStatement.setObject(7, phone.getId());
            preparedStatement.executeUpdate();
            resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                int savedId = resultSet.getInt(1);
                id = new Integer(savedId);
            }
        } catch (SQLException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        } finally {
            closeConnection(resultSet, preparedStatement, connection);
        }
        return id;
    }

    private Phone mapModelFromResultSet(ResultSet resultSet) throws SQLException {
        Phone phone = new Phone();
        phone.setId(resultSet.getInt("id"));
        phone.setCountryCode(resultSet.getString("country_code"));
        phone.setOperatorCode(resultSet.getString("operator_code"));
        phone.setPhone(resultSet.getString("phone"));
        phone.getPhoneType().setId(resultSet.getInt("phone_type_id"));
        phone.setComment(resultSet.getString("comment"));
        phone.setContactId(resultSet.getInt("contact_id"));
        return phone;
    }

}