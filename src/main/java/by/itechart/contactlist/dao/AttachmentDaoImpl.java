package by.itechart.contactlist.dao;

import by.itechart.contactlist.dao.interfaces.AttachmentDao;
import by.itechart.contactlist.manager.ConnectionManager;
import by.itechart.contactlist.model.Attachment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class AttachmentDaoImpl extends AbstractDao implements AttachmentDao {

    private final Logger log = LoggerFactory.getLogger(AttachmentDaoImpl.class);
    private ConnectionManager connectionPool = ConnectionManager.getInstance();

    @Override
    public List<Attachment> getAll() {
        List<Attachment> attachmentList = new ArrayList<>();
        Statement statement = null;
        ResultSet resultSet = null;
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT * FROM attachment;");
            while (resultSet.next()) {
                Attachment attachment = mapModelFromResultSet(resultSet);
                attachmentList.add(attachment);
            }
        } catch (SQLException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        } finally {
            closeConnection(resultSet, statement, connection);
        }
        return attachmentList;
    }

    @Override
    public Attachment get(Integer id) {
        if (Objects.isNull(id)) {
            return null;
        }
        Attachment attachment = new Attachment();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            preparedStatement = connection.prepareStatement("SELECT * FROM attachment where id=?;");
            preparedStatement.setInt(1, id);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                attachment = mapModelFromResultSet(resultSet);
            }
        } catch (SQLException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        } finally {
            closeConnection(resultSet, preparedStatement, connection);
        }
        return attachment;
    }

    @Override
    public Integer save(Attachment attachment) {
        if(isNew(attachment)){
            return persist(attachment);
        } else {
            return update(attachment);
        }
    }

    @Override
    public void saveAll(Iterable<Attachment> attachments) {
        if (attachments == null) {
            return ;
        }
        for (Attachment attachment : attachments) {
            save(attachment);
        }
    }

    @Override
    public void delete(Integer id){
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            preparedStatement = connection.prepareStatement("DELETE FROM attachment where id=?;");
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        } finally {
            closeConnection(preparedStatement, connection);
        }
    }

    @Override
    public void deleteByContactId(Integer id){
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            preparedStatement = connection.prepareStatement("DELETE FROM attachment where contact_id=?;");
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        } finally {
            closeConnection(preparedStatement, connection);
        }
    }

    @Override
    public boolean isEmpty() {
        boolean isEmpty = true;
        Statement statement = null;
        ResultSet resultSet = null;
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT * FROM attachment;");
            if (resultSet.next()) {
                isEmpty = false;
            }
        } catch (SQLException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        } finally {
            closeConnection(resultSet, statement, connection);
        }
        return isEmpty;
    }

    private Integer persist(Attachment attachment) {
        if(Objects.isNull(attachment)){
            return null;
        }
        Integer id = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
                String insertSQL = "INSERT INTO attachment (path, upload_date, comment, contact_id) VALUES(?,?,?,?);";
                preparedStatement = connection.prepareStatement(insertSQL, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, attachment.getPath());
            preparedStatement.setObject(2, attachment.getUploadDate());
            preparedStatement.setString(3, attachment.getComment());
            preparedStatement.setObject(4, attachment.getContactId());
            preparedStatement.executeUpdate();
            resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                int savedId = resultSet.getInt(1);
                id = new Integer(savedId);
            }
        } catch (SQLException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        } finally {
            closeConnection(resultSet, preparedStatement, connection);
        }
        return id;
    }

    private Integer update(Attachment attachment) {
        if(Objects.isNull(attachment)){
            return null;
        }
        Integer id = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            String updateSQL = "UPDATE attachment SET path=?, upload_date=?, comment=?, contact_id=? WHERE id=?;";
            preparedStatement = connection.prepareStatement(updateSQL, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, attachment.getPath());
            preparedStatement.setObject(2, attachment.getUploadDate());
            preparedStatement.setString(3, attachment.getComment());
            preparedStatement.setObject(4, attachment.getContactId());
            preparedStatement.setObject(5, attachment.getId());
            preparedStatement.executeUpdate();
            resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                int savedId = resultSet.getInt(1);
                id = new Integer(savedId);
            }
        } catch (SQLException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        } finally {
            closeConnection(resultSet, preparedStatement, connection);
        }
        return id;
    }

    private Attachment mapModelFromResultSet(ResultSet resultSet) throws SQLException {
        Attachment attachment = new Attachment();
        attachment.setId(resultSet.getInt("id"));
        attachment.setPath(resultSet.getString("path"));
        attachment.setUploadDate(resultSet.getDate("upload_date"));
        attachment.setComment(resultSet.getString("comment"));
        attachment.setContactId(resultSet.getInt("contact_id"));
        return attachment;
    }

}