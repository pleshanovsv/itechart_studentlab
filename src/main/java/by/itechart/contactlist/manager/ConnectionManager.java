package by.itechart.contactlist.manager;

import org.apache.commons.dbcp2.BasicDataSource;

import java.sql.Connection;
import java.sql.SQLException;

public class ConnectionManager {
    private static ConnectionManager ourInstance = new ConnectionManager();
    private static BasicDataSource basicDataSource = new BasicDataSource();
    private static ConfigurationManager configurationManager = ConfigurationManager.getInstance();

    static {
        basicDataSource.setUrl(configurationManager.getDataBaseUrl());
        basicDataSource.setUsername(configurationManager.getDataBaseUsername());
        basicDataSource.setPassword(configurationManager.getDataBasePassword());
        basicDataSource.setMinIdle(configurationManager.getMinIdle());
        basicDataSource.setMaxIdle(configurationManager.getMaxIdle());
        basicDataSource.setMaxOpenPreparedStatements(configurationManager.getMaxOpenPreparedStatements());
    }

    private ConnectionManager() {
    }

    public static ConnectionManager getInstance() {
        return ourInstance;
    }

    public Connection getConnection() throws SQLException {
        Connection connection = basicDataSource.getConnection();
        return connection;
    }

}
