package by.itechart.contactlist.manager;

import java.util.ResourceBundle;

public class ConfigurationManager {
    private static ConfigurationManager ourInstance = new ConfigurationManager();
    private ResourceBundle resourceBundle = ResourceBundle.getBundle("application");

    private ConfigurationManager() {
    }

    public static ConfigurationManager getInstance() {
        return ourInstance;
    }

    private String getString(String key) {
        return resourceBundle.getString(key);
    }

    public String getDataBaseDriver() {
        String value = getString("postgres.driver");
        return value;
    }

    public String getDataBaseUrl() {
        String value = getString("postgres.url");
        return value;
    }

    public String getDataBaseUsername() {
        String value = getString("postgres.username");
        return value;
    }

    public String getDataBasePassword() {
        String value = getString("postgres.password");
        return value;
    }

    public int getMinIdle() {
        String value = getString("basicDataSource.minIdle");
        int intValue = Integer.parseInt(value);
        return intValue;
    }

    public int getMaxIdle() {
        String value = getString("basicDataSource.maxIdle");
        int intValue = Integer.parseInt(value);
        return intValue;
    }

    public int getMaxOpenPreparedStatements() {
        String value = getString("basicDataSource.maxOpenPreparedStatements");
        int intValue = Integer.parseInt(value);
        return intValue;
    }


}
