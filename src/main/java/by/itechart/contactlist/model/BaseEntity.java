package by.itechart.contactlist.model;

public class BaseEntity<T> {
    private T id;

    public T getId() {
        return id;
    }

    public void setId(T id) {
        this.id = id;
    }

    @Override
    public String toString() {
        String className = getClass().getSimpleName().toLowerCase();
        return className;
    }

}
