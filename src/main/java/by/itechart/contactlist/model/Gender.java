package by.itechart.contactlist.model;

public class Gender extends BaseEntity<Integer> {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
