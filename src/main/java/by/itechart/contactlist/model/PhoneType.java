package by.itechart.contactlist.model;

public class PhoneType extends BaseEntity<Integer> {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "phone_type";
    }
}
