DROP DATABASE IF EXISTS contactlist;
CREATE DATABASE contactlist;
-- USE contactlist;
\c contactlist;

CREATE TABLE gender 
(
id serial,  
name character varying(15),
CONSTRAINT gender_pkey PRIMARY KEY (id)
);

CREATE TABLE phone_type
(
id serial,  
name character varying(15),
CONSTRAINT phone_type_pkey PRIMARY KEY (id)
);

CREATE TABLE contact 
(
id serial,  
first_name character varying(50),
last_name character varying(50),
middle_name character varying(50),
birth_day date,
gender_id integer,
citizenship character varying(50),
family_status character varying(20),
web_site character varying(255),
email character varying(255),
company character varying(50),
CONSTRAINT contact_pkey PRIMARY KEY (id),
CONSTRAINT gender_id_fkay FOREIGN KEY (gender_id) REFERENCES gender (id)
);

CREATE TABLE address
(
id serial,
country character varying(40),
city character varying(30),
street character varying(30),
house character varying(4),
apartment character varying(5),
postcode character varying(16),
contact_id integer,
CONSTRAINT address_pkey PRIMARY KEY (id),
CONSTRAINT contact_id_fkay FOREIGN KEY (contact_id) REFERENCES contact (id)
);

CREATE TABLE phone 
(
id serial,  
country_code character varying(3),
operator_code character varying(4),
phone character varying(15),
phone_type_id integer,
comment character varying(255),
contact_id integer,
CONSTRAINT phone_pkey PRIMARY KEY (id),
CONSTRAINT contact_id_fkay FOREIGN KEY (contact_id) REFERENCES contact (id),
CONSTRAINT phone_type_id_fkay FOREIGN KEY (phone_type_id) REFERENCES phone_type (id)
);

CREATE TABLE attachment
(
id serial,  
path character varying(255),
upload_date date,
contact_id integer,
comment character varying(255),
CONSTRAINT attachment_pkey PRIMARY KEY (id),
CONSTRAINT contact_id_fkay FOREIGN KEY (contact_id) REFERENCES contact (id)
);

